package com.ellendan.servicetemplate.web.exception.entity;

public interface IErrorCode {
    String code();
    int httpStatus();
}
