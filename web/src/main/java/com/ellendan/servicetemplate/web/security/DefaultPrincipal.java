package com.ellendan.servicetemplate.web.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class DefaultPrincipal implements CurrentPrincipal{
    private final Long accountId;
    private final String appId;
    @Override
    public String getName() {
        return this.accountId.toString();
    }
}
