package com.ellendan.servicetemplate.web.exception.handler;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResult {
    private final String code;
    private final String message;
    private final Object errorData;

    public ErrorResult(String code, String message) {
        this.code = code;
        this.message = message;
        this.errorData = null;
    }

    public ErrorResult(String code, String message, Object errorData) {
        this.code = code;
        this.message = message;
        this.errorData = errorData;
    }
}
