package com.ellendan.servicetemplate.web.security;

import org.springframework.security.oauth2.jwt.Jwt;

public interface CurrentPrincipalFactory {
    CurrentPrincipal newObject(Jwt jwt);
}
