package com.ellendan.servicetemplate.web.exception.entity;

import lombok.Getter;

@Getter
public class BusinessException extends CustomizedException {
    private final Object errorData;

    public BusinessException(IErrorCode errorCode, String message, Object errorData) {
        super(errorCode, message);
        this.errorData = errorData;
    }
}
