package com.ellendan.servicetemplate.web.exception.entity;

public enum ApplicationErrorCode implements IErrorCode {
    INTERNAL_SERVICE_ERROR("internal_service_error", 500);

    private final String code;
    private final int httpStatus;

    ApplicationErrorCode(String code, int httpStatus) {
        this.code = code;
        this.httpStatus = httpStatus;
    }


    @Override
    public String code() {
        return this.code;
    }

    @Override
    public int httpStatus() {
        return this.httpStatus;
    }
}
