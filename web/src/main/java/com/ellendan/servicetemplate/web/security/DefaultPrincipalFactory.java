package com.ellendan.servicetemplate.web.security;

import org.springframework.security.oauth2.jwt.Jwt;

public class DefaultPrincipalFactory implements CurrentPrincipalFactory {
    @Override
    public DefaultPrincipal newObject(Jwt jwt) {
        return DefaultPrincipal.builder()
                .accountId(Long.parseLong(jwt.getSubject()))
                .appId(jwt.getClaimAsString("aid"))
                .build();
    }
}
