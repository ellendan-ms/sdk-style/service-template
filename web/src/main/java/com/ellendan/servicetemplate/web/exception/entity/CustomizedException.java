package com.ellendan.servicetemplate.web.exception.entity;

import lombok.Getter;

@Getter
public class CustomizedException extends RuntimeException {
    private final IErrorCode errorCode;

    public CustomizedException(IErrorCode errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public CustomizedException(IErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }
}
