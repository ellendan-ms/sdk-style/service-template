package com.ellendan.servicetemplate.web.security;

import org.springframework.security.core.AuthenticatedPrincipal;

public interface CurrentPrincipal extends AuthenticatedPrincipal {
}
