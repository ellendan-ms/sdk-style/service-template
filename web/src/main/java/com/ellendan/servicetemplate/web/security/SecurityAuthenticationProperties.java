package com.ellendan.servicetemplate.web.security;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Data
@ConfigurationProperties(prefix = "servicetemplate.web.security")
public class SecurityAuthenticationProperties {
    private String jwtPub;
    private String jwtKey;
    private IgnoreProperties ignore;

    @Data
    static class IgnoreProperties {
        private Boolean antMatcherEnabled;
        private List<String> paths;

        public String[] getPaths() {
            return paths.toArray(new String[0]);
        }
    }
}
