package com.ellendan.servicetemplate.web.exception.handler;

import com.ellendan.servicetemplate.web.exception.entity.ApplicationErrorCode;
import com.ellendan.servicetemplate.web.exception.entity.ApplicationException;
import com.ellendan.servicetemplate.web.exception.entity.BusinessException;
import com.ellendan.servicetemplate.web.exception.entity.CustomizedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class InterfaceExceptionHandler {

    @ExceptionHandler({BusinessException.class, ApplicationException.class})
    public ResponseEntity<ErrorResult> handlerException(CustomizedException exception) {
        log.info(String.format("[%s] %s", exception.getErrorCode().code(), exception.getMessage()),
                exception);
        Object errorData = null;
        if(exception instanceof BusinessException) {
            errorData = ((BusinessException) exception).getErrorData();
        }
        ErrorResult result = new ErrorResult(exception.getErrorCode().code(),
                exception.getMessage(), errorData);
        return new ResponseEntity(result, HttpStatus.valueOf(exception.getErrorCode().httpStatus()));
    }

    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ErrorResult> handlerException(Throwable exception) {
        log.info(exception.getMessage(), exception);

        ErrorResult result = new ErrorResult(ApplicationErrorCode.INTERNAL_SERVICE_ERROR.code(),
                exception.getMessage());
        return new ResponseEntity(result, HttpStatus.valueOf(ApplicationErrorCode
                .INTERNAL_SERVICE_ERROR.httpStatus()));
    }
}
