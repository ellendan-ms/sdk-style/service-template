package com.ellendan.servicetemplate.web;

import com.ellendan.servicetemplate.web.exception.handler.InterfaceExceptionHandler;
import com.ellendan.servicetemplate.web.security.SecurityConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({SecurityConfig.class,
    InterfaceExceptionHandler.class})
public class WebAutoConfiguration {
}
