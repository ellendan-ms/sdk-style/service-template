package com.ellendan.servicetemplate.web.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.AbstractOAuth2TokenAuthenticationToken;

import java.util.Collection;
import java.util.Map;

public class CustomizedJwtAuthenticationToken extends AbstractOAuth2TokenAuthenticationToken<Jwt> {
    protected CustomizedJwtAuthenticationToken(Jwt token, Object principal,
                                               Collection<? extends GrantedAuthority> authorities) {
        super(token, principal, token, authorities);
        this.setAuthenticated(true);
    }

    @Override
    public Map<String, Object> getTokenAttributes() {
        return this.getToken().getClaims();
    }
}
