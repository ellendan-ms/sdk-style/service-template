package com.ellendan.servicetemplate.web.exception.entity;

import lombok.Getter;

@Getter
public class ApplicationException extends CustomizedException {
    public ApplicationException(String message, Throwable e) {
        super(ApplicationErrorCode.INTERNAL_SERVICE_ERROR, message, e);
    }
}
