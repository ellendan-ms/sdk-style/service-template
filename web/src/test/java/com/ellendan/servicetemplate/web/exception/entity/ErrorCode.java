package com.ellendan.servicetemplate.web.exception.entity;

public enum ErrorCode implements IErrorCode {
    CUSTOMER_NOT_FOUND("customer_not_found", 404),
    CUSTOMER_CONFLICT("customer_conflict", 409);
    private final String code;
    private final int httpStatus;

    ErrorCode(String code, int httpStatus) {
        this.code = code;
        this.httpStatus = httpStatus;
    }

    @Override
    public String code() {
        return this.code;
    }

    @Override
    public int httpStatus() {
        return this.httpStatus;
    }
}
