package com.ellendan.servicetemplate.web.exception.handler;

import com.ellendan.servicetemplate.web.exception.entity.ApplicationException;
import com.ellendan.servicetemplate.web.exception.entity.BusinessException;
import com.ellendan.servicetemplate.web.exception.entity.ErrorCode;
import com.google.common.collect.ImmutableMap;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static com.ellendan.servicetemplate.web.exception.entity.ApplicationErrorCode.INTERNAL_SERVICE_ERROR;

class InterfaceExceptionHandlerTest {
    @Test
    void shouldHandlerBusinessExceptionCorrectly() {
        InterfaceExceptionHandler handler = new InterfaceExceptionHandler();

        BusinessException bizException = new BusinessException(
                ErrorCode.CUSTOMER_CONFLICT, "customer conflict",
                ImmutableMap.of("conflict_customer_id", "p123")
        );
        ResponseEntity<ErrorResult> response = handler.handlerException(bizException);
        Assertions.assertThat(response)
                .hasFieldOrPropertyWithValue("body",
                        new ErrorResult(bizException.getErrorCode().code(),
                        bizException.getMessage(), bizException.getErrorData()))
                .hasFieldOrPropertyWithValue("status", HttpStatus.CONFLICT);
    }

    @Test
    void shouldHandlerApplicationExceptionCorrectly() {
        InterfaceExceptionHandler handler = new InterfaceExceptionHandler();

        ApplicationException applicationException = new ApplicationException(
                "customer conflict", new RuntimeException("mock exception")
        );
        ResponseEntity<ErrorResult> response = handler.handlerException(applicationException);
        Assertions.assertThat(response)
                .hasFieldOrPropertyWithValue("body",
                        new ErrorResult(applicationException.getErrorCode().code(),
                                applicationException.getMessage()))
                .hasFieldOrPropertyWithValue("status", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    void shouldHandlerOtherExceptionCorrectly() {
        InterfaceExceptionHandler handler = new InterfaceExceptionHandler();

        Exception exception = new NullPointerException("mock test message");
        ResponseEntity<ErrorResult> response = handler.handlerException(exception);
        Assertions.assertThat(response)
                .hasFieldOrPropertyWithValue("body",
                        new ErrorResult(INTERNAL_SERVICE_ERROR.code(),
                                exception.getMessage()))
                .hasFieldOrPropertyWithValue("status", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}