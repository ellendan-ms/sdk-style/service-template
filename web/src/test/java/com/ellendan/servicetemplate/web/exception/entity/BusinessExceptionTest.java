package com.ellendan.servicetemplate.web.exception.entity;

import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

class BusinessExceptionTest {

    @Test
    void shouldCreateBusinessExceptionCorrectly() {
        String errorMessage = "Person p123 not found";
        Map errorResult = ImmutableMap.of("customer_id", "p");
        CustomizedException exception = new BusinessException(ErrorCode.CUSTOMER_CONFLICT,
                errorMessage, errorResult);
        assertThat(exception).hasMessage(errorMessage).hasNoNullFieldsOrProperties();
    }

}