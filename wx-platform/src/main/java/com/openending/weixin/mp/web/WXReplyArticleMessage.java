package com.openending.weixin.mp.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class WXReplyArticleMessage extends WXReplyMessage {
  private final Integer articleCount;
  private final List<Article> articles;

  public WXReplyArticleMessage(String toUserName, String fromUserName, List<Article> articles) {
    super(toUserName, fromUserName, new Date().getTime() + "", MsgType.NEWS);
    this.articles = articles;
    this.articleCount = articles.size();
  }

  @Override
  public String toXMLString() {
    String format = "<xml>"
        + "<ToUserName><![CDATA[%1$s]]></ToUserName>"
        + "<FromUserName><![CDATA[%2$s]]></FromUserName>"
        + "<CreateTime>%3$s</CreateTime>"
        + "<MsgType><![CDATA[%4$s]]></MsgType>"
        + "<ArticleCount>%5$s</ArticleCount>"
        + "<Articles>%6$s</Articles>"
        + "</xml>";
    return String.format(format, toUserName, fromUserName, createTime, msgType.toValue(),
        articleCount, articles.stream().map(Article::toXMLString).collect(Collectors.joining()));
  }

  @Data
  @Builder
  @AllArgsConstructor
  public static class Article {
    private String title;
    private String description;
    private String picUrl;
    private String url;

    public String toXMLString() {
      String format = "<item>"
          + "<Title><![CDATA[%1$s]]></Title>"
          + "<Description><![CDATA[%2$s]]></Description>"
          + "<PicUrl><![CDATA[%3$s]]></PicUrl>"
          + "<Url><![CDATA[%4$s]]></Url>"
          + "</item>";
      return String.format(format, title, description, picUrl, url);
    }
  }
}
