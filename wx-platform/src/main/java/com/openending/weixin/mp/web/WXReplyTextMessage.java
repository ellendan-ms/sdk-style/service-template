package com.openending.weixin.mp.web;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class WXReplyTextMessage extends WXReplyMessage {
  private String content;

  public WXReplyTextMessage(String toUserName, String fromUserName, String content) {
    super(toUserName, fromUserName, new Date().getTime() + "", MsgType.TEXT);
    this.content = content;
  }

  public String toXMLString() {
    String format = "<xml>"
        + "<ToUserName><![CDATA[%1$s]]></ToUserName>"
        + "<FromUserName><![CDATA[%2$s]]></FromUserName>"
        + "<CreateTime>%3$s</CreateTime>"
        + "<MsgType><![CDATA[%4$s]]></MsgType>"
        + "<Content><![CDATA[%5$s]]></Content>"
        + "</xml>";
    return String.format(format, toUserName, fromUserName, createTime, msgType.toValue(), content);
  }
}
