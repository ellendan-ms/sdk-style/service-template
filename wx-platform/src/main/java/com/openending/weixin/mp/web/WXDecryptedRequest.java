package com.openending.weixin.mp.web;

import lombok.Data;

@Data
public class WXDecryptedRequest {
  private final String toUserName;
  private final WXDecryptedRequestContent content;
}
