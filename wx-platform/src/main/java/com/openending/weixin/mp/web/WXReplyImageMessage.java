package com.openending.weixin.mp.web;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class WXReplyImageMessage extends WXReplyMessage {
  private String imageId;

  public WXReplyImageMessage(String toUserName, String fromUserName, String imageId) {
    super(toUserName, fromUserName, new Date().getTime() + "", MsgType.IMAGE);
    this.imageId = imageId;
  }

  public String toXMLString() {
    String format = "<xml>"
        + "<ToUserName><![CDATA[%1$s]]></ToUserName>"
        + "<FromUserName><![CDATA[%2$s]]></FromUserName>"
        + "<CreateTime>%3$s</CreateTime>"
        + "<MsgType><![CDATA[%4$s]]></MsgType>"
        + "<Image>"
        + "<MediaId><![CDATA[%5$s]]></MediaId>"
        + "</Image>"
        + "</xml>";
    return String.format(format, toUserName, fromUserName, createTime, msgType, imageId);
  }
}
