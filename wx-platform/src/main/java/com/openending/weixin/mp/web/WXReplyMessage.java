package com.openending.weixin.mp.web;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public abstract class WXReplyMessage {
  protected String toUserName;
  protected String fromUserName;
  protected String createTime;
  protected MsgType msgType;

  public abstract String toXMLString();
}
