package com.openending.weixin.mp.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class WXDecryptedRequestContent {
  @JsonProperty("ToUserName")
  private String toUserName;
  @JsonProperty("FromUserName")
  private String fromUserName;
  @JsonProperty("CreateTime")
  private Long createTime;
  @JsonProperty("MsgType")
  private MsgType msgType;
  @JsonProperty("Event")
  private Event event;
  @JsonProperty("EventKey")
  private String eventKey;
  @JsonProperty("List")
  private List<Map> list;
  @JsonProperty("debug_str")
  private String debugStr;
}
