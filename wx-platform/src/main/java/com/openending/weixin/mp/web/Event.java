package com.openending.weixin.mp.web;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Event {
  DEBUG_DEMO,
  SUBSCRIBE,
  UNSUBSCRIBE,
  CLICK,
  VIEW,
  LOCATION;
  @JsonCreator
  public static Event fromValue(String value) {
    for (Event event : values()) {
      if (event.name().equalsIgnoreCase(value)) {
        return event;
      }
    }
    throw new IllegalArgumentException("Unknown enum event: " + value);
  }

  @JsonValue
  public String toValue() {
    return name().toLowerCase();
  }
}

