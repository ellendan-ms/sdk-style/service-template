package com.openending.weixin.mp.web;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.openending.weixin.mp.aes.AesException;
import com.openending.weixin.mp.aes.WXBizMsgCrypt;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Slf4j
public class DecryptXMLHandlerInterceptor implements HandlerInterceptor {
  private final WXBizMsgCrypt wxBizMsgCrypt;
  private final XmlMapper xmlMapper = new XmlMapper();
  public static final String WX_DECRYPTED_REQUEST_ATTRIBUTE_NAME = "WX_DECRYPTED_REQUEST";
  public static final String WX_VERIFY_ECHO= "WX_VERIFY_ECHO";

  public DecryptXMLHandlerInterceptor(WXBizMsgCrypt wxBizMsgCrypt) {
    this.wxBizMsgCrypt = wxBizMsgCrypt;
  }

  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    if (!(handler instanceof HandlerMethod)) {
      return true;
    }
    HandlerMethod method = (HandlerMethod) handler;
    MethodParameter[] methodParameters = method.getMethodParameters();
    for (MethodParameter methodParameter : methodParameters) {
      RequestAttribute foundAnnotation = methodParameter.getParameterAnnotation(RequestAttribute.class);
      if (foundAnnotation == null) {
        continue;
      }

      if (foundAnnotation.name().equals(WX_VERIFY_ECHO)) {
        setEchoString(request);
      } else if (foundAnnotation.name().equals(WX_DECRYPTED_REQUEST_ATTRIBUTE_NAME)) {
        setWXDecryptedRequestAttribute(request);
      }
    }
    return true;
  }

  private void setEchoString(HttpServletRequest request) throws AesException {
    String signature = request.getParameter("signature");
    String timestamp = request.getParameter("timestamp");
    String nonce = request.getParameter("nonce");
    String echoString = request.getParameter("echostr");

    this.wxBizMsgCrypt.verifySignature(signature, timestamp, nonce, null);
    request.setAttribute(WX_VERIFY_ECHO, echoString);
  }

  private void setWXDecryptedRequestAttribute(HttpServletRequest request) throws IOException, AesException {
    String msgSignature = request.getParameter("msg_signature");
    String timestamp = request.getParameter("timestamp");
    String nonce = request.getParameter("nonce");
    log.info("msgSignature: {}, timestamp: {}, nonce: {}", msgSignature, timestamp, nonce);
    String requestXmlBody = request.getReader().lines().reduce("", (accumulator, actual) -> accumulator + actual);
    log.info(requestXmlBody);

    List<String> decryptedBody = this.wxBizMsgCrypt.decryptMsg(msgSignature, timestamp, nonce, requestXmlBody);
    WXDecryptedRequestContent wxDecryptedRequestContent = xmlMapper.readValue(decryptedBody.get(1), WXDecryptedRequestContent.class);
    WXDecryptedRequest wxDecryptedRequest = new WXDecryptedRequest(decryptedBody.get(0), wxDecryptedRequestContent);
    request.setAttribute(WX_DECRYPTED_REQUEST_ATTRIBUTE_NAME, wxDecryptedRequest);
  }
}
