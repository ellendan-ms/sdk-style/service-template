package com.openending.weixin.mp.web;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

@Getter
public enum MsgType {
  EVENT,
  TEXT,
  IMAGE,
  NEWS;

  @JsonCreator
  public static MsgType fromValue(String value) {
    for (MsgType type : values()) {
      if (type.name().equalsIgnoreCase(value)) {
        return type;
      }
    }
    throw new IllegalArgumentException("Unknown enum type: " + value);
  }

  @JsonValue
  public String toValue() {
    return name().toLowerCase();
  }
}
