package com.ellendan.service.template.example.infrastructure.client;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Map;

@CircuitBreaker(name="remote")
@Retry(name="remote")
@FeignClient(name = "remoteClient", url = "127.0.0.1:31002")
public interface RemoteFeignClient {

    @GetMapping("/products/{productId}")
    Map<String, String> fetchProduct(@PathVariable("productId") String productId);
}
