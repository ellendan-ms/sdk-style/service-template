package com.ellendan.service.template.example.interfaces;

import com.ellendan.service.template.example.infrastructure.client.RemoteFeignClient;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


@RestController
@RequiredArgsConstructor
@RequestMapping("/test")
public class ExampleController {
    private final RemoteFeignClient remoteFeignClient;

    @GetMapping("/{productId}")
    Map<String, String> getProduct(@PathVariable String productId) {
        return remoteFeignClient.fetchProduct(productId);
    }
}
