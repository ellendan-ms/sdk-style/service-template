# servicestemplate

### usage

```
repositories {
    mavenCentral()
    maven { url "https://gitlab.com/api/v4/projects/39170322/packages/maven" }
}
dependencies {
    implementation platform('com.ellendan.service-template:dependencies-bom:<version>')
}
```

### directories introduction

`service-template-project:dependencies-bom`: manage the BOM definition and publish.  
`example`: use the service-template-projects for a service.  

### Launch infrastructure
```
docker-compose -f docker-compose.yml up -d
```
